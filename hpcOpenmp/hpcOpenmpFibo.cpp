
#include <fstream>
#include <iostream>
#include <thread>
#include <vector>

#include <omp.h>

// calcule le Nieme terme de la suite de "Fibonacci modulo 42"
// precondition : N >= 0
int FibonacciMod42(int N)
{
    int f_curr = 0;
    int f_prec = 1;
    for (int i=1; i<=N; i++)
    {
        int tmp = f_curr;
        f_curr = (f_curr + f_prec) % 42;
        f_prec = tmp;
    }
    return f_curr;
}

int main(int argc, char ** argv)
{
    // verifie les parametres de la ligne de commande
    if (argc != 2)
    {
        std::cout << "usage: " << argv[0] << " <nbData>" << std::endl;
        return -1;
    }


    // calcule le tableau de donnees
    int nbData = std::stoi(argv[1]);
    std::vector<int> data2(nbData); 
    double startTime = omp_get_wtime();
    // TODO
     #pragma omp parallel for schedule(static, 1) 
        for (int i=0; i<nbData; i++)
        {
            data2[i] = FibonacciMod42(i);
        }

     double endTime = omp_get_wtime();
    std::cout<< "With OMP : " << endTime - startTime << std::endl;


    // calcule le tableau de donnees
    
    std::vector<int> data(nbData); 
    startTime = omp_get_wtime();
    // TODO
    
        for (int i=0; i<nbData; i++)
        {
            data[i] = FibonacciMod42(i);
        }
     endTime = omp_get_wtime();
    std::cout<< "Without OMP : " << endTime - startTime << std::endl;

    



    // ecrit les donnees calculees dans un fichier
    std::ofstream ofs("output.txt");
    for (int x : data)
    {
        ofs << x << ' ';
    }



    return 0;
}

